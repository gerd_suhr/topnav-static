"use strict";

var config = {
    app: {
        indexFile: 'index.html',
        mainJsFile: 'app.js'
    },

    // source directories
    sass_src: './src/scss/**/*.scss',
    less_src: './src/less/theme/main.less',
    html_src: './src/**/*.html',
    css_src: './src/css/**/*.css',
    js_src: './src/js/**/*.js',
    image_src: './src/images/**/*',
    font_src: './src/fonts/**/*',
    

    // output directory 
    dest: "dist",

    // dev server 
    dev: {
        protocol: 'http',
        host: 'localhost',
        port: '9006',
        proxy: {
            port: '7777'
        }
    },
};

config.paths = {
    html: config.html_src, // Matches all html files in current folder
    css: config.css_src, // Matches all css files, recursively
    less: config.less_src, // Matches all less files, recursively
    js: config.js_src, // Matches all js files, recursively
    image: config.image_src, // Matches all image files, recursively
    fonts: config.font_src, // Matches all image files, recursively
};

module.exports = config;