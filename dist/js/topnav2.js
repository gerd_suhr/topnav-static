var topNav2 = {
  $subNav: null,
  $subNavLinks: null,
  $subNavLinksLevel3: null,
  $overlay: null,
  $mobileSearchContainer: null,
  sticky: false,
  $el: null,
  hasSelectedItem: false,
  $inputSearch: null,

  init: function () {
      //init vars
      this.$subNav = $('.topnav2__nav__container__items__item--industries__subnav');
      this.$subNavLinks = $('.topnav2__nav__container__items__item--industries__subnav__item__link');
      this.$subNavLinksLevel3 = $('.topnav2__nav__container__items__item--industries__subnav__item__third-level');
      this.$subGlobalNav = $('.topnav2__nav__container__items__item--global__subnav');
      this.$overlay = $('.topnav2__nav__overlay');
      this.$mobileSearchContainer = $('.topnav2__nav__container__search--mobile');
      this.$el = $('.topnav2__nav');
      this.$inputSearch = $('.topnav2__nav__container__search__form__input');
      this.hasSelectedItem = this.$el.data('selectedItem');

  // slide in menu elements
      this.$menuTrigger = $('#jsMenuToggle'); // the ID of burger
      this.$mobileSlide = $('#jsMobileSlide'); // the ID of top (outer) UL
      this.$submenu = $('.jsOpenNext'); // goes on all hrefs tha have a subnav UL
      this.$closeSubNav = $('.jsCloseSub'); // closes the subnavs
      this.$closeSlideMenu = $('.jsCloseSlideMenu'); //closes the slide menu
      this.slideSubNav = $('.jsSlideSubNav');

  // Hide nav on scroll down and show on scroll up 
      this.didScroll = false;
      this.lastScrollTop = 0;
      this.navbarHeight = $(this.$el).outerHeight();
      this.delta = this.navbarHeight;

      $(window).scroll(function(event){
          topNav2.didScroll = true;
      });
      setInterval(function() { 
          if (topNav2.didScroll) {
              topNav2.hasScrolled();
              topNav2.didScroll = false;
          }
      }, 200);
  // end hide nav on scroll down and show on scroll up  

      $('html, body').animate({
          scrollTop: 0
      }, 0);

      /*  Init the the DL_MENU flyin-in navigation plugin. Used on DESKTOP view only*/
      $('#dl-menu').dlmenu({
          animationClasses: { classin: 'dl-animate-in-2',  classout: 'dl-animate-out-2' }
      });
      // Desktop megamenu opener
      $('.topnav2__nav__container__items__item--industries__link').on('click', topNav2.onMainMenuButtonClick);
      
      // Desktop global dropdown menu click event
      $('.topnav2__nav__container__items__item--global__link').on('click', topNav2.onGlobalMainButtonClick);

      // Desktop center navigation back link click event. Hide all divs.
      $('ul.dl-menu li .dl-back').on('click', function(){
          $('.mega-menu__left-content__links>div').removeClass('active').addClass('inactive');
      });


      // SLIDE-IN EVENT HANDLERS
      // Mobile slide-in menu click togggle
      topNav2.$menuTrigger.on('click', function(e) {
          if (topNav2.$mobileSlide.hasClass('slideisOpen')) {
              topNav2.closeMobileSlideNav();
          } else {
              topNav2.openMobileSlideNav();
          }
      });

      // Close the entire mobile slide-in panel 
      $(this.$closeSlideMenu).on('click', function(){
          topNav2.closeMobileSlideNav();
      });

      // Mobile slide-in list item click
      $(topNav2.$submenu).on('click.slide-in', topNav2.mobileSlideInItemClick);

      // Mobile Slide-in list item back button click
      $(this.$closeSubNav).on('click',  topNav2.mobileSlideInBackClick);

      // TILES EVENT HANDLERS
      // Open the mobiles slide-in switch tiles menu 
      $('.open-switcher-mobile').on('click', topNav2.toggleSwitchIndustryMenu);
      // Close the mobile switch tiles menu 
      $('#close-switcher').on('click',topNav2.toggleSwitchIndustryMenu);
      // Mobile switch industry item click 
      $('#switch-tiles-mobile li').on('click', topNav2.mobileSwitchIndustryTile);

  // Search form stuff ------------------------------------------------------------------------ 
      $(".topnav2__nav__container__search__form").each(function (index) {
          var action = $(this).data('searchpage') + '.html';
          $(this).attr('action', action);
      });

      topNav2.$inputSearch.keyup(function (index) {
          var action = $(this).parent().data('searchpage');
          var q = $(this).val();
          if (q === "")
              action += '.q.html';
          else
              action += '.html?SearchTerm=' + q;
          $(this).parent().attr('action', action);
      });

      // Mobile button search
      $('.topnav2__nav__container--mobile__items__item--search, .topnav2__nav__container--mobile__items__item--search__icon, #fake-input ').on('click', function (e) {
          if (e.target !== this) {
              return;
          }
          topNav2.toggleSearch();
          topNav2.$inputSearch.first().focus();
      });

      $('#search-close').on('click', function(){
          topNav2.toggleSearch();
      });
  },


  openMobileSlideNav: function () {
      // add .isOpen class to the top (outer) two UL's. This slides in the panel and shows top level links.
      topNav2.$mobileSlide.addClass('slideisOpen');
      //$(topNav2.$mobileSlide).find('ul').addClass('slideisOpen');
      $('body').addClass('mobileNavIsOpen');
  },

  closeMobileSlideNav: function () {
      topNav2.$mobileSlide.removeClass('slideisOpen');
      $('.topnav2__mobile-slide-panel__list').find('ul').removeClass('slideisOpen');
      topNav2.$submenu.siblings().removeClass('slideisOpen');
      $('body').removeClass('mobileNavIsOpen');
  },
    
  // open/close the switch tiles menu
toggleSwitchIndustryMenu: function(){
      event.stopPropagation();
      //$('#topnav2-mobile-overlay').toggleClass('topnav2__nav__overlay--mobile--visible');
      $('#switch-tiles-mobile').toggleClass('topnav2__switch-tiles__mobile--hidden');
  },
  
  resetMobileSlideNav: function(){
      $('.topnav2__mobile-slide-panel__list ul').removeClass('slideisOpen');
      //hide the industry headers
      $('[id^=sh-]').not('.hidden').addClass('hidden');
      // show the home header.
       $('#sh-pallhome').removeClass('hidden'); 
  },

  mobileSwitchIndustryTile: function(){
      event.stopPropagation();
      topNav2.resetMobileSlideNav();
      var text = $(this).attr('data-industry');
      // find matching slide-in item link
      var $industry =	$('.topnav2__nav__container--mobile__subnav__item').children('a[data-industry="'+text+'"]');
      // trigger nav click
      $($industry).trigger('click.slide-in');
      // close the tile menu
      $('#switch-tiles-mobile').toggleClass('topnav2__switch-tiles__mobile--hidden'); 
  },

  mobileSlideInItemClick: function() {
      // open subnavs
      $(this).next(this.slideSubNav).addClass('slideisOpen');
      // open first nested subnav
      $($(this).next('ul')).find('ul').first().addClass('slideisOpen');
      // Change header for clicked industry 
      var industry = $(this).attr('data-industry');
      industry = industry.trim();
      // set all mobile slide-in headers to class .hidden
      $('[id^=sh-]').not('.hidden').addClass('hidden');
      // remove class .hidden mobile slide-in header for clicked industry
      $('#sh-'+industry).removeClass('hidden');
  },
  
  mobileSlideInBackClick: function(){
      $(this).closest('ul').removeClass('slideisOpen');
      // are we back to top level check.
      if ( $('#top-industry').hasClass('slideisOpen') ){
          // industry sub-menu still open so not at top level
      } else {
          //hide the industry headers
          $('[id^=sh-]').not('.hidden').addClass('hidden');
          // show the home header.
          $('#sh-pallhome').removeClass('hidden');
      }
  },

  toggleSearch: function () {
      topNav2.$mobileSearchContainer.toggleClass('active');
  },
  // open the desktop mega-menu
  onMainMenuButtonClick: function (event) {
      event.preventDefault();
      $('#mega-menu').toggleClass('megaOpen');
  },
  // global dropdown menu
  onGlobalMainButtonClick: function (event) {
      event.preventDefault();
      topNav2.$subNav.removeClass('active');
      topNav2.$subGlobalNav.toggleClass('active');
      topNav2.$subNavLinksLevel3.removeClass('active');
  },

  // show menu on scroll-up function
  hasScrolled: function () {
      var scrolltop = $(window).scrollTop();
      // Make sure they scroll more than delta
      if(Math.abs(topNav2.lastScrollTop - scrolltop) <= topNav2.delta)	return;

      // If they scrolled down and are past the navbar, add class hide.
      // This is necessary so you never see what is "behind" the navbar.
      if (scrolltop > topNav2.lastScrollTop && scrolltop > topNav2.delta){
        // On scroll down we add hide
        $(topNav2.$el).removeClass('show topnav2__nav--sticky').addClass('hide');

      } else {
        // On scroll up we add show
        if(scrolltop + $(window).height() < $(document).height()) {    
            if( scrolltop < 20) { 
            // if were all the way back up, switch back to relative positioning
            $(topNav2.$el).addClass('show').removeClass('topnav2__nav--sticky');
            } else {
                console.log(topNav2.lastScrollTop);
                $(topNav2.$el).removeClass('hide').addClass('show topnav2__nav--sticky');
            }
        }
      }

      topNav2.lastScrollTop = scrolltop;
  },

  // needToInit: function () {
  //     return $('.topnav2__nav').length > 0;
  // } 
};

topNav2.init();