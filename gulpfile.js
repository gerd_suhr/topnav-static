/* ===============================================================================
** A gulp workflow to use ES6 with export/import and all the goodies of ES6 
** We need browserify to transpile (wrap) our javascript for CommonJS synchronous loading 
** so that browsers will understand it. 
==================================================================================*/
var gulp = require('gulp'),
		// prints the gulp tasks if you want to see them working
		print = require('gulp-print'),
		// allows us to delete files/folders
		del = require('del'), 
		// allows us to run gulp tasks in specific sequence
		runSq = require('run-sequence'), 
		// creates wrappper around our JS so we can us modules and the browser understands it.
		browserify = require("browserify"),
		// babelify transpiles our JS to ES6 (with Browserify we need babelify not babel, See .babelrc file)
		babelify = require("babelify"),
		// we need viny-source-stream to be able to pipe within the browserify task
		source = require('vinyl-source-stream'),
		// gulp-util is a logging tool for debuggin you gulp tasks
		gutil = require('gulp-util'),
		liveServer = require('gulp-live-server'); // Lightweight local dev server
		// transpile our scss code to css
		less = require('gulp-less'),
		// autoprefix our css for across browsers
		autoprefixer = require('gulp-autoprefixer'), 
		 //concatenate files
		concat = require('gulp-concat'),
		// clean and minify css
		cleanCss = require('gulp-clean-css'),
		// uses css sourcemaps
		sourcemaps = require('gulp-sourcemaps'),
		// lint our JavaScript
		jshint = require('gulp-jshint');
		// minify javascript
		uglify = require('gulp-uglify'), 
		// we need streamify to be able to pipe uglify in browserify task
		streamify = require('gulp-streamify'), 
		// allows us to specifify the order to concatenate our css
		order = require("gulp-order"),
		// browser-sync dev server (hot reloading with watch)
		browserSync = require('browser-sync'),
    // Project's configuration data (pathing, etc.)
		config = require('./app.config')

 
// delete everyting in the dist folder
gulp.task('clean', () => {
	return del([  config.dest + '/**/*',  ]) 
});

gulp.task('js', () => {
	return gulp.src(config.js_src) // glob the folder structure
	.pipe(jshint())
  .pipe(jshint.reporter('jshint-stylish'))
	.pipe(gulp.dest(config.dest+'/js/'))
	.pipe(browserSync.stream());
});

// Convert less into css
gulp.task('less', () => {
	return gulp.src(config.paths.less)
		.pipe(print())
		.pipe(sourcemaps.init()) // use source maps
		.pipe(less())
		.pipe(autoprefixer()) // prefix the css
		.pipe(sourcemaps.write('./maps')) // save source maps here
		//.pipe(concat(config.css.bundleFile))
		.pipe(gulp.dest(config.dest+'/css/')) // save css here
		.pipe(browserSync.stream());
});


// concatenate and minify all our 3rd party vendor css into 1 file.
gulp.task('vendor-css', () => {
	return gulp.src(config.paths.css)
	//.pipe(print())
					.pipe(autoprefixer()) // prefix the css
					.pipe(order([
						"css/boostrap3.js",
						"gulp css/animate.css",
						//"./css/**/font-awesome.css"
					]))
					.pipe(concat('vendor.css')) // concatenate into 1 file called vendor.css
					.pipe(cleanCss()) // minify the css
					.pipe(gulp.dest(config.dest+'/css/')) // save it here
					.pipe(browserSync.stream());
})


// copy all CSS files to dist folder
gulp.task('css', () => {
	return gulp.src(config.paths.css) // glob the folder structure
	//.pipe(print())
	.pipe(gulp.dest(config.dest+'/css/'))
	.pipe(browserSync.stream());
});

// copy all html files to dist folder
gulp.task('html', () => {
	return gulp.src(config.paths.html) // glob the folder structure
	//.pipe(print())
	.pipe(gulp.dest(config.dest))
	.pipe(browserSync.stream());
});


// copy all image files to dist/images folder
gulp.task('images', () => {
	return gulp.src(config.paths.image) // glob the folder structure
	//.pipe(print())
	.pipe(gulp.dest(config.dest + '/images/'))
	.pipe(browserSync.stream());
})

// copy all font files to dist/images folder
gulp.task('fonts', () => {
	return gulp.src(config.paths.fonts) // glob the folder structure
	//.pipe(print())
	.pipe(gulp.dest(config.dest + '/fonts/'))
	.pipe(browserSync.stream());
})


// Start a local dev server
gulp.task('live-server', function() {
	var server = liveServer.static(config.dest, config.dev.proxy.port);
	server.start();
});

// Run live reload using BrowserSync
gulp.task('serve', ['live-server'], function() {
	browserSync.init(null, {// 'null': we already have our server setup
			proxy: config.dev.protocol + '://' + config.dev.host + ':' + config.dev.proxy.port, // server URL
			port: config.dev.port // port for the new connection
	});
});

// Watch file changes
gulp.task('watch', function() {
	gulp.watch(config.paths.html, ['html']); // Run the "html" task every time a file is modified in the html dir
	gulp.watch(config.paths.less, ['less']); // Run the "less" task every time a less file is modified
	gulp.watch(config.paths.css, ['css']); // Run the "css" task every time a css file is modified
	gulp.watch(config.paths.js, ['js']); // Run the "js" task every time a js file is modified
	gulp.watch(config.paths.image, ['images']); // Run the "images" task every time a image file is modified
	gulp.watch(config.paths.fonts, ['fonts']); // Run the "fonts" task every time a font file is modified
});


// This task will fire up all the other tasks in the build
gulp.task('all', ['html', 'less', 'css', 'js', 'images', 'fonts'], function() {
	gulp.start('watch');
	gulp.start('serve');
});

// run these tasks in sequence using run-sequence and launce the dev server
// gulp.task('dev', function() {
// 	runSq('cleanOut', ['copy-html', 'copy-images', 'vendor-css'], ['less', 'copyJS','server']);
// });


// gulp.task('default', function() {
// 	runSq('clean', ['all']);
// });

// Use for Production build.
gulp.task('build', function() {
	runSq('clean', ['html', 'less', 'css', 'js', 'images', 'fonts']);
});


